﻿
using System.Data.Entity;
using SmsCrud.Model;

namespace SmsCrud.Repository
{
   public class EfConnection: DbContext
    {
        public DbSet<Content> Contents { get; set; }
        public DbSet<Service> Serviceses { get; set; }
        public DbSet<ContentData> ContentsDatas { get; set; }
        public DbSet<Subscriptions> Subscriptions { get; set; }

    }
}
