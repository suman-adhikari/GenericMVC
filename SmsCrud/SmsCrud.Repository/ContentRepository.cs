﻿using System.Data.Entity.Core.Objects;
using SmsCrud.Model;

namespace SmsCrud.Repository
{
    public class ContentRepository:Repo<Content>
    {
        public ContentRepository(DbContextGenerartor generartor) : base(generartor)
        {
        }
    }
}
