﻿namespace SmsCrud.Repository
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
