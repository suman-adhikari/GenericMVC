﻿using System.Data.Entity;

namespace SmsCrud.Repository
{
    public class DbContextGenerartor : IDbContextGenerartor
    {
        private readonly EfConnection _efConnection;
        public DbContextGenerartor(EfConnection efConnection)
        {
            _efConnection = efConnection;
        }
        public DbContext GetContext()
        {
            return _efConnection;
        }
    }
}
