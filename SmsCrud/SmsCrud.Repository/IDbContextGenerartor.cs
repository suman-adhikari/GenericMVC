﻿using System.Data.Entity;

namespace SmsCrud.Repository
{
    public interface IDbContextGenerartor
    {
        DbContext GetContext();
    }
}