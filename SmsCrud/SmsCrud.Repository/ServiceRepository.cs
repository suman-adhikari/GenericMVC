﻿
using SmsCrud.Model;

namespace SmsCrud.Repository
{
   public class ServiceRepository : Repo<Service> 
   {
       public ServiceRepository(DbContextGenerartor dbContextGenerartor)
           : base(dbContextGenerartor)
       {
       }
   }
}
