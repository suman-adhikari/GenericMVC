﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Script.Serialization;
using SmsCrud.Model;

namespace SmsCrud.Repository
{
    public class Repo<T> where T : class
    {
        private readonly DbContext _dbContext;
        

        public Repo(DbContextGenerartor generartor){
            _dbContext = generartor.GetContext();
        }

        public bool Save(T entity)
        {
            try
            {
                _dbContext.Set<T>().Add(entity);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool Delete(int id)
        {
            try
            {
                T entity = _dbContext.Set<T>().Find(id);
                _dbContext.Set<T>().Remove(entity);
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }

        }

        public T Edit(int id)
        {
            try
            {
                T entity = _dbContext.Set<T>().Find(id);
                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
                
            }
        }

        public bool SaveEdit(T entity)
        {
            try
            {
                _dbContext.Entry(entity).State = EntityState.Modified;
                _dbContext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AjaxGridResult FindAll(int offset, int rowNumber, string sortExpression, string sortOrder, int pageNumber)
        {
            try
            {
                var jsearalizer = new JavaScriptSerializer();

                List<T> c = new List<T>();

                c = _dbContext.Set<T>().ToList();
                object o = c;

             //  var data = jsearalizer.Serialize(c);

              //  string jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(employee);

                var data = Newtonsoft.Json.JsonConvert.SerializeObject(o);
               
                return new AjaxGridResult
                {
                    Data = data,
                    PageNumber = 1,
                    RowCount = 10
                };
            }
            catch (Exception ex)
            {
                throw ex;
                return new AjaxGridResult();
            }
        }
    }
}
