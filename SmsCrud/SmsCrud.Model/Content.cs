﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmsCrud.Model
{
    [Table("Content")]
    public class Content
    {
        public int Id { get; set; }

        public string WorkName { get; set; }

        public string DefaultName { get; set; }

        public Boolean Active { get; set; }

        public Boolean IsManual { get; set; }
    }
}
