﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmsCrud.Model
{
   public class AjaxGridResult
    {
        public int RowCount { get; set; }

        public string Data { get; set; }

        public int PageNumber { get; set; }
    }
}
