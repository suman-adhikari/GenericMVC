﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;

namespace SmsCrud.Model
{

  [Table("ContentData")]
   public class ContentData : IEntity
    {
       public int Id { get; set; }

     
      public int ContentID { get; set; }

     // [XmlIgnore]
     // public DateTime DatetimeInserted { get; set; }

      public DateTime DatetimeInserted
      {
          get { return DateTime.Now; }
          set { value = DateTime.Now; }
      }

      public int Language { get; set; }

       public string Text { get; set; }

       public int SpecificParam { get; set; }

   //// [XmlElement("DatetimeInserted")]
   //// public string SomeDateString
   // {
   //     get { return this.DatetimeInserted.ToString("yyyy-MM-dd HH:mm:ss"); }
   //     set { this.DatetimeInserted = DateTime.Parse(value); }
   // }
      
    }
}
