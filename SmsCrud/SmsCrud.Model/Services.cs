﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmsCrud.Model
{
   public class Service
    {
       public int ID { get; set; }

       public int OperatorID { get; set; }

       public int ContentID { get; set; }

       public string ContentName { get; set; }

       public string Sender { get; set; }

       public int SMSGateID { get; set; }

       public int FOCsms { get; set; }

       public int LanguageBitMask { get; set; }

       public int BillingGateID { get; set; }

       public int FireTime { get; set; }

       public int ContentGenerationType { get; set; }

       public Boolean SMSonTrialEnd { get; set; }

       public string TrialEndSMSText { get; set; }

       public int EndTime { get; set; }



       
     
     

    }
}
