﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmsCrud.Model
{
    public class Subscriptions
    {
        public int ID { get; set; }
        public int ServiceID { get; set; }
        public int OperatorID { get; set; }

        public int ContentID { get; set; }
       

        public string MSISDN { get; set; }

        public int SpecificParam { get; set; }

        public DateTime DateRegistered { get; set; }

        public DateTime DateStopped { get; set; }


        public string Status { get; set; }

        public string ActivationChannel { get; set; }

        public int FreeSMS { get; set; }


        public int Marker { get; set; }

        public int ActivationNumber { get; set; }
    }
}
