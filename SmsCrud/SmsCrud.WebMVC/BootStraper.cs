﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.Web;
using SmsCrud.Repository;

namespace SmsCrud.WebMVC
{
    public class BootStraper
    {
        private static IContainerProvider _containerProvider;

        public static IContainerProvider ConfigureDependencies()
        {
            return SetAutofacContainer();
        }

        private static IContainerProvider SetAutofacContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.Load("SmsCrud.WebMVC"));

            

            builder.RegisterType<ContentRepository>().As<ContentRepository>().InstancePerLifetimeScope();
            builder.RegisterType<ContentDataRepository>().As<ContentDataRepository>().InstancePerLifetimeScope();
            builder.RegisterType<ServiceRepository>().As<ServiceRepository>().InstancePerLifetimeScope();
            builder.RegisterType<EfConnection>().As<EfConnection>().InstancePerLifetimeScope();
            builder.RegisterType<DbContextGenerartor>().As<DbContextGenerartor>().InstancePerLifetimeScope();
           

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            _containerProvider = new ContainerProvider(container);

            return _containerProvider;
        }
    }
}