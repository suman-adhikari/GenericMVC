﻿using System.Web;
using System.Web.Optimization;

namespace SmsCrud.WebMVC
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/includes/scripts/jquery-1.7.2.js",
                        "~/includes/scripts/ajaxGrid.js",
                         "~/includes/scripts/Shared.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/includes/scripts/jquery-ui-1.10.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/includes/scripts/jquery.unobtrusive*",
                        "~/includes/scripts/jquery.validationEngine.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

          

            bundles.Add(new StyleBundle("~/Styles/css").Include(
                      "~/includes/styles/jquery-ui.css",
                      "~/includes/styles/bootstrap-theme.css",
                      "~/includes/styles/bootstrap.css",
                      "~/includes/styles/validationEngine.jquery.css",
                      "~/includes/styles/style.css"
                        ));
        }
    }
}