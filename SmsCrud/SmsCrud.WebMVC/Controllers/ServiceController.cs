﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SmsCrud.Model;
using SmsCrud.Repository;

namespace SmsCrud.WebMVC.Controllers
{
    public class ServiceController : MainController<Service>
    {

        public ServiceController(ServiceRepository serviceRepository) : base(serviceRepository)
        {
        }

    }
}
