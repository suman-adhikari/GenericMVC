﻿using SmsCrud.Model;
using SmsCrud.Repository;

namespace SmsCrud.WebMVC.Controllers
{
    public class ContentController : MainController<Content>
    {

        public ContentController(ContentRepository contentRepository): base(contentRepository)
        {
          
        }
    }
}