﻿using System.Web.Mvc;
using SmsCrud.Repository;

namespace SmsCrud.WebMVC.Controllers
{
    public abstract class MainController<T> : Controller where T : class
    {

        private readonly Repo<T> _repo; 

        protected MainController(Repo<T> repo )
        {
            _repo = repo;
        }

        [HttpPost]
        public ActionResult Index(T t)
        {
            bool result = _repo.Save(t);
            return View();
            //return Json(new
            //{
            //    Result = result ? "Success" : "Failed",
            //    Message = result ? "Sucessfully Added User" : "Error occured while Deleting"
            //});
        }

        public JsonResult DeleteUser(int id)
        {
           bool result =  _repo.Delete(id);
            return Json(new
            {
                Result = result ? "Success" : "Failed",
                Message = result ? "Sucessfully Deleted User" : "Error occured while Deleting"
            });
        }

        public ActionResult EditUser(int id)
        {
            var model = _repo.Edit(id);
            return View(model);
        }


        public JsonResult SaveEdit(T t)
        {
            bool result = _repo.SaveEdit(t);

            return Json(new 
            {
                Result = result ? "Success" : "Failed",
                Message = result ? "Sucessfully Edited User" : "Error occured while Deleting"
            });
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult FindAll(int offset, int rowNumber, string sortExpression, string sortOrder, int pageNumber)
        {
            return Json(_repo.FindAll(offset, rowNumber, sortExpression, sortOrder, pageNumber));
        }

    }
}
