﻿using SmsCrud.Model;
using SmsCrud.Repository;

namespace SmsCrud.WebMVC.Controllers
{
    public class ContentDataController : MainController<ContentData>
    {
        public ContentDataController(ContentDataRepository contentDataRepository) : base(contentDataRepository)
        {

        }

    }
}
